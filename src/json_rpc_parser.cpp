/*
 * Copyright (c) 2012, Nasel Security
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "json_rpc_parser.hpp"
#include "json_rpc_method.hpp"

#include <string>
#include <iostream>
#include <vector>

#include <boost/algorithm/string/predicate.hpp>

#include <json/reader.h>
#include <json/writer.h>

namespace json_rpc {


bool JsonRpcParser::parse(std::istream& input, std::string& output) {
    Json::FastWriter writer;
    Json::Value read_json;
    try {
        input >> read_json;
    }
    catch (std::exception& e) {
        output = writer.write(prepare_error(-32700, "Parse error."));
        return true;
    }

    Json::Value response;
    bool must_respond;
    if (read_json.isArray()) {
        must_respond = parse_batch(read_json, response);
    }
    else {
        must_respond = parse_request(read_json, response);
    }

    if (must_respond)
        output = writer.write(response);
    return must_respond;

}

bool JsonRpcParser::parse_request(const Json::Value& request, Json::Value& response) {

    if (!validate_rpc(request)) {
        response = prepare_error(-32600, "Invalid Request.");
        return true;
    }

    std::string method_name = request["method"].asString();
    Json::Value id;
    if (request.isMember("id"))
        id = request["id"];

    method_map::iterator method;
    if ((method = methods_.find(method_name)) == methods_.end()) {
        response = prepare_error(-32601, "Method not found.", id);
        return true;
    }

    Json::Value params;
    if (request.isMember("params"))
        params = request["params"];

    Json::Value result;
    try {
        method->second(params, result);
    }
    catch (InvalidParameters& e) {
        response = prepare_error(-32602, "Invalid params.", id);
        return true;
    }
    catch (...) {
        response = prepare_error(-32603, "Internal error.", id);
        return true;
    }
    if (request.isMember("id"))
        response = prepare_response(result, id);
    return request.isMember("id");

}

bool JsonRpcParser::parse_batch(const Json::Value& request, Json::Value& response) {

    if (request.size() == 0) {
        response = prepare_error(-32600, "Invalid Request.");
        return true;
    }

    Json::Value response_list(Json::arrayValue);
    bool is_response = false;

    for (const auto &req : request) {
        Json::Value tmp_resp;
        bool is_tmp_resp;
        is_tmp_resp = parse_request(req, tmp_resp);
        if (is_tmp_resp)
            response_list.append(tmp_resp);
        is_response |= is_tmp_resp;
    }

    response = response_list;
    return is_response;

}

void JsonRpcParser::add_method(const std::string& method_name, JsonRpcMethod method_body) {
    methods_.insert(method_map::value_type(method_name, method_body));

}

Json::Value JsonRpcParser::prepare_error(int32_t error_code, std::string error_message, Json::Value id) {
    Json::Value root;

    root["jsonrpc"] = "2.0";
    root["error"]["code"] = error_code;
    root["error"]["message"] = error_message;
    root["id"] = id;
    return root;

}

Json::Value JsonRpcParser::prepare_response(const Json::Value& result, Json::Value id) {
    Json::Value root;

    root["jsonrpc"] = "2.0";
    root["result"] = result;
    root["id"] = id;

    return root;

}

bool JsonRpcParser::validate_rpc(const Json::Value& root) {
    if (!(root.isMember("jsonrpc") && root["jsonrpc"] == "2.0")) {
        return false;
    }
    Json::Value method = root["method"];
    if (!method)
        return false;

    if (!method.isString())
        return false;

    if (root.isMember("params") && !(root["params"].isObject() || root["params"].isArray()))
        return false;

    if (root.isMember("id") && !(root["id"].isString() || root["id"].isNumeric() || root["id"].isNull()))
        return false;

    return true;
}

} /* namespace json_rpc */
