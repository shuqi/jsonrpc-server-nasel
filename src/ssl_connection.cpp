/*
 * Copyright (c) 2012, Nasel Security
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef SSL_CONNECTION_CPP_
#define SSL_CONNECTION_CPP_

#include "ssl_connection.hpp"
#include "json_rpc_parser.hpp"

#include <iostream>
#include <stdint.h>
#include <ctime>

#include <boost/asio/ssl.hpp>
#include <boost/bind.hpp>
//#include <boost/date_time/posix_time/posix_time.hpp>

namespace json_rpc {

SslConnection::SslConnection(boost::asio::io_service& io_service,
        boost::asio::ssl::context& ctx,
        uint64_t msecs_per_transmission,
        uint64_t msecs_per_connection,
        JsonRpcParser& rpc_parser) :
    socket_(io_service, ctx),
    strand_(io_service),
    transmission_timer_(io_service),
    connection_timer_(io_service),
    rpc_parser_(rpc_parser) {
    msec_pc_ = boost::posix_time::milliseconds(msecs_per_connection);

}

ssl_socket::lowest_layer_type& SslConnection::socket() {
    return socket_.lowest_layer();
}

void SslConnection::set_off() {
    socket_.async_handshake(ssl_socket::server, strand_.wrap(
            boost::bind(&SslConnection::handle_handshake,
                    shared_from_this(),
                    boost::asio::placeholders::error)));

    connection_timer_.expires_from_now(msec_pc_);

}

void SslConnection::handle_handshake(const boost::system::error_code& ec) {
    boost::asio::async_read_until(
            socket_,
            data_buffer_,
            '\x00',
            strand_.wrap(boost::bind(
                    &SslConnection::handle_read,
                    shared_from_this(),
                    boost::asio::placeholders::error)));
}

void SslConnection::handle_read(const boost::system::error_code& ec) {
    std::istream is(&data_buffer_);
    std::string result;
    if (rpc_parser_.parse(is, result)) {
        result.push_back('\x00');
        boost::asio::async_write(
                socket_,
                boost::asio::buffer(result),
                boost::bind(
                        &SslConnection::handle_write,
                        shared_from_this(),
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
    }

}

void SslConnection::handle_write(const boost::system::error_code& ec, size_t bytes_transferred) {

}

}
#endif
/* SSL_CONNECTION_CPP_ */
