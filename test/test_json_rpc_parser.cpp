/*
 * Copyright (c) 2012, Nasel Security
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>
#include <iostream>
#include <sstream>

#include <gtest/gtest.h>
#include <json/json.h>

#include <json_rpc_parser.hpp>
#include <json_rpc_method.hpp>

#define __null 0

class JsonRpcParserTest : public ::testing::Test {
public:

    void SetUp() {
        json_rpc::JsonRpcMethod method_ok(
                JsonRpcParserTest::always_validate,
                JsonRpcParserTest::write_params_to_response);
        json_rpc::JsonRpcMethod method_not_ok(
                        JsonRpcParserTest::never_validate,
                        JsonRpcParserTest::write_params_to_response);
        json_rpc::JsonRpcMethod method_exception(
                        JsonRpcParserTest::always_validate,
                        JsonRpcParserTest::throw_exception);
        parser.add_method("method_ok", method_ok);
        parser.add_method("method_not_ok", method_not_ok);
        parser.add_method("method_exception", method_exception);
    }

protected:
    json_rpc::JsonRpcParser parser;
private:


    static bool never_validate(const Json::Value& params) {
        return false;
    }

    static bool always_validate(const Json::Value& params) {
        return true;
    }

    static void write_params_to_response(const Json::Value& params, Json::Value& response) {
        response = params;
    }

    static void throw_exception(const Json::Value& params, Json::Value& response) {
        throw std::exception();
    }
};

TEST_F(JsonRpcParserTest, InvalidJsonString) {

    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::objectValue);
    expected_response["id"] = Json::Value(Json::nullValue);
    expected_response["jsonrpc"] = Json::Value("2.0");
    expected_response["error"]["code"] = Json::Value(-32700);
    expected_response["error"]["message"] = Json::Value("Parse error.");

    std::istringstream not_close_brace("{\"a\":\"b\"", std::istringstream::in);
    EXPECT_TRUE(parser.parse(not_close_brace, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream not_close_bracket("{\"a\":\"b\", \"c\":[1,2}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(not_close_bracket, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream missing_quotes("{a\":\"b\", \"c\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(missing_quotes, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream missing_comma("{\"a\":\"b\" \"c\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(missing_comma, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

}

TEST_F(JsonRpcParserTest, InvalidJsonRpc) {

    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::objectValue);
    expected_response["id"] = Json::Value(Json::nullValue);
    expected_response["jsonrpc"] = Json::Value("2.0");
    expected_response["error"]["code"] = Json::Value(-32600);
    expected_response["error"]["message"] = Json::Value("Invalid Request.");

    std::istringstream whithout_jsonrpc("{\"method\":\"method_ok\", \"id\":1, \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(whithout_jsonrpc, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_jsonrpc_type("{\"jsonrpc\":2.0, \"method\":\"method_ok\", \"id\":1, \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_jsonrpc_type, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_jsonrpc_string("{\"jsonrpc\":\"2.1\", \"method\":\"method_ok\", \"id\":1, \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_jsonrpc_string, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_method_type_1("{\"jsonrpc\":\"2.0\", \"method\":3, \"id\":1, \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_method_type_1, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_method_type_2("{\"jsonrpc\":\"2.0\", \"method\":[1,2], \"id\":1, \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_method_type_2, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_method_type_3("{\"jsonrpc\":\"2.0\", \"method\":{\"method\":\"method_ok\"}, \"id\":1, \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_method_type_3, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_params_type_1("{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"id\":1, \"params\":1}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_params_type_1, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_params_type_2("{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"id\":1, \"params\":2.0}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_params_type_2, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_params_type_3("{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"id\":1, \"params\":\"params\"}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_params_type_3, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_id_type_1("{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"id\":[1], \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_id_type_1, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream wrong_id_type_2("{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"id\":{\"id\": 1}, \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(wrong_id_type_2, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

}

TEST_F(JsonRpcParserTest, InexistentMethod) {

    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::objectValue);
    expected_response["id"] = Json::Value(Json::nullValue);
    expected_response["jsonrpc"] = Json::Value("2.0");
    expected_response["error"]["code"] = Json::Value(-32601);
    expected_response["error"]["message"] = Json::Value("Method not found.");

    std::istringstream without_id("{\"jsonrpc\":\"2.0\", \"method\":\"i_do_not_exist\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(without_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value(1);
    std::istringstream with_int_id("{\"jsonrpc\":\"2.0\", \"id\": 1, \"method\":\"i_do_not_exist\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_int_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value("1");
    std::istringstream with_string_id("{\"jsonrpc\":\"2.0\", \"id\": \"1\", \"method\":\"i_do_not_exist\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_string_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value(Json::nullValue);
    std::istringstream with_null_id("{\"jsonrpc\":\"2.0\", \"id\": null, \"method\":\"i_do_not_exist\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_null_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

}

TEST_F(JsonRpcParserTest, WrongParameters) {

    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::objectValue);
    expected_response["id"] = Json::Value(Json::nullValue);
    expected_response["jsonrpc"] = Json::Value("2.0");
    expected_response["error"]["code"] = Json::Value(-32602);
    expected_response["error"]["message"] = Json::Value("Invalid params.");

    std::istringstream without_id("{\"jsonrpc\":\"2.0\", \"method\":\"method_not_ok\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(without_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value(1);
    std::istringstream with_int_id("{\"jsonrpc\":\"2.0\", \"id\": 1, \"method\":\"method_not_ok\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_int_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value("1");
    std::istringstream with_string_id("{\"jsonrpc\":\"2.0\", \"id\": \"1\", \"method\":\"method_not_ok\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_string_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value(Json::nullValue);
    std::istringstream with_null_id("{\"jsonrpc\":\"2.0\", \"id\": null, \"method\":\"method_not_ok\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_null_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

}

TEST_F(JsonRpcParserTest, MethodException) {

    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::objectValue);
    expected_response["id"] = Json::Value(Json::nullValue);
    expected_response["jsonrpc"] = Json::Value("2.0");
    expected_response["error"]["code"] = Json::Value(-32603);
    expected_response["error"]["message"] = Json::Value("Internal error.");

    std::istringstream without_id("{\"jsonrpc\":\"2.0\", \"method\":\"method_exception\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(without_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value(1);
    std::istringstream with_int_id("{\"jsonrpc\":\"2.0\", \"id\": 1, \"method\":\"method_exception\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_int_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value("1");
    std::istringstream with_string_id("{\"jsonrpc\":\"2.0\", \"id\": \"1\", \"method\":\"method_exception\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_string_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value(Json::nullValue);
    std::istringstream with_null_id("{\"jsonrpc\":\"2.0\", \"id\": null, \"method\":\"method_exception\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_null_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

}

TEST_F(JsonRpcParserTest, RequestOkWithId) {
    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::objectValue);
    expected_response["jsonrpc"] = Json::Value("2.0");
    expected_response["result"].append(1);
    expected_response["result"].append(2);

    expected_response["id"] = Json::Value(1);
    std::istringstream with_int_id("{\"jsonrpc\":\"2.0\", \"id\": 1, \"method\":\"method_ok\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_int_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value("1");
    std::istringstream with_string_id("{\"jsonrpc\":\"2.0\", \"id\": \"1\", \"method\":\"method_ok\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_string_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    expected_response["id"] = Json::Value(Json::nullValue);
    std::istringstream with_null_id("{\"jsonrpc\":\"2.0\", \"id\": null, \"method\":\"method_ok\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_TRUE(parser.parse(with_null_id, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);
}

TEST_F(JsonRpcParserTest, RequestOkWithoutId) {
    std::string output;

    std::istringstream input("{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"params\":[1,2]}", std::istringstream::in);
    EXPECT_FALSE(parser.parse(input, output));

}

TEST_F(JsonRpcParserTest, BatchWithErrors) {
    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::arrayValue);
    Json::Value tmp_response;
    tmp_response["id"] = Json::Value(Json::nullValue);
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["error"]["code"] = Json::Value(-32603);
    tmp_response["error"]["message"] = Json::Value("Internal error.");
    expected_response.append(tmp_response);
    tmp_response.clear();
    tmp_response["id"] = Json::Value("1");
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["error"]["code"] = Json::Value(-32601);
    tmp_response["error"]["message"] = Json::Value("Method not found.");
    expected_response.append(tmp_response);
    tmp_response.clear();
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["result"].append(1);
    tmp_response["result"].append(2);
    tmp_response["id"] = Json::Value(20);
    expected_response.append(tmp_response);

    std::istringstream batch_req("[{\"jsonrpc\":\"2.0\", \"method\":\"method_exception\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"id\": \"1\", \"method\":\"i_do_not_exist\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"id\": 20, \"method\":\"method_ok\", \"params\":[1,2]}]", std::istringstream::in);
    EXPECT_TRUE(parser.parse(batch_req, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

}

TEST_F(JsonRpcParserTest, BatchWithoutErrors) {

    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::arrayValue);
    Json::Value tmp_response;
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["result"].append(1);
    tmp_response["result"].append(2);
    tmp_response["id"] = Json::Value("1");
    expected_response.append(tmp_response);
    tmp_response.clear();
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["result"].append(1);
    tmp_response["result"].append(2);
    tmp_response["id"] = Json::Value(2);
    expected_response.append(tmp_response);
    tmp_response.clear();
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["result"].append(1);
    tmp_response["result"].append(2);
    tmp_response["id"] = Json::Value(20);
    expected_response.append(tmp_response);

    std::istringstream batch_req("[{\"jsonrpc\":\"2.0\", \"id\": \"1\", \"method\":\"method_ok\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"id\": 2, \"method\":\"method_ok\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"id\": 20, \"method\":\"method_ok\", \"params\":[1,2]}]", std::istringstream::in);
    EXPECT_TRUE(parser.parse(batch_req, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

}

TEST_F(JsonRpcParserTest, BatchWithNotifications) {

    Json::Reader reader;
    std::string output;
    Json::Value response;

    Json::Value expected_response(Json::arrayValue);
    Json::Value tmp_response;
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["result"].append(1);
    tmp_response["result"].append(2);
    tmp_response["id"] = Json::Value("1");
    expected_response.append(tmp_response);
    tmp_response.clear();
    tmp_response["id"] = Json::Value("1");
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["error"]["code"] = Json::Value(-32601);
    tmp_response["error"]["message"] = Json::Value("Method not found.");
    expected_response.append(tmp_response);
    tmp_response.clear();
    tmp_response["jsonrpc"] = Json::Value("2.0");
    tmp_response["result"].append(1);
    tmp_response["result"].append(2);
    tmp_response["id"] = Json::Value(20);
    expected_response.append(tmp_response);

    std::istringstream batch_req("[{\"jsonrpc\":\"2.0\", \"id\": \"1\", \"method\":\"method_ok\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"id\": \"1\", \"method\":\"i_do_not_exist\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"id\": 20, \"method\":\"method_ok\", \"params\":[1,2]}]", std::istringstream::in);
    EXPECT_TRUE(parser.parse(batch_req, output));
    reader.parse(output, response);
    EXPECT_EQ(expected_response, response);

    std::istringstream only_notif("[{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"params\":[1,2]},"
             "{\"jsonrpc\":\"2.0\", \"method\":\"method_ok\", \"params\":[1,2]}]", std::istringstream::in);
    EXPECT_FALSE(parser.parse(only_notif, output));


}
