/*
 * Copyright (c) 2012, Nasel Security
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdexcept>

#include <gtest/gtest.h>
#include <json/json.h>

#include "json_rpc_method.hpp"

#define __null 0

class JsonRpcMethodTest : public ::testing::Test {
public:
    static bool never_validate(const Json::Value& params) {
        return false;
    }

    static bool always_validate(const Json::Value& params) {
        return true;
    }

    static bool validate_if_expected(const Json::Value& params) {
        return JsonRpcMethodTest::params_test == params;
    }

    static void throw_exception(const Json::Value& params, Json::Value& response) {
        throw std::exception();
    }

    static void write_params_to_response(const Json::Value& params, Json::Value& response) {
        response = params;
    }

    static Json::Value params_test;

protected:

    virtual void SetUp() {
        JsonRpcMethodTest::params_test.append(Json::Value(1));
        JsonRpcMethodTest::params_test.append(Json::Value(2));
        JsonRpcMethodTest::params_test.append(Json::Value("third"));
    }

    virtual void TearDown() {
        JsonRpcMethodTest::params_test.clear();
    }


private:


};

Json::Value JsonRpcMethodTest::params_test;

TEST_F(JsonRpcMethodTest, NeverValidationCheck) {
    json_rpc::JsonRpcMethod not_validate(
            JsonRpcMethodTest::never_validate,
            JsonRpcMethodTest::write_params_to_response);
    Json::Value params;
    Json::Value response;
    params.append(Json::Value(1));
    ASSERT_THROW(not_validate(params, response), json_rpc::InvalidParameters);

}

TEST_F(JsonRpcMethodTest, AlwaysValidationCheck) {
    json_rpc::JsonRpcMethod validate(
            JsonRpcMethodTest::always_validate,
            JsonRpcMethodTest::write_params_to_response);
    Json::Value params;
    Json::Value response;
    params.append(Json::Value(1));
    ASSERT_NO_THROW(validate(params, response));
    params.clear();
    ASSERT_NO_THROW(validate(params, response));

}

TEST_F(JsonRpcMethodTest, DifferValidationCheck) {
    json_rpc::JsonRpcMethod diff_validate(
            JsonRpcMethodTest::validate_if_expected,
            JsonRpcMethodTest::write_params_to_response);
    Json::Value params;
    Json::Value response;
    params.append(Json::Value(1));
    ASSERT_THROW(diff_validate(params, response), json_rpc::InvalidParameters);
    params.clear();

    params.append(Json::Value(1));
    params.append(Json::Value(2));
    params.append(Json::Value("thrd"));
    ASSERT_THROW(diff_validate(params, response), json_rpc::InvalidParameters);
    params.clear();

    params.append(Json::Value(1));
    params.append(Json::Value(2));
    params.append(Json::Value("third"));
    ASSERT_NO_THROW(diff_validate(params, response));

}

TEST_F(JsonRpcMethodTest, CorrectMethodCall) {
    json_rpc::JsonRpcMethod exception_raiser(
            JsonRpcMethodTest::always_validate,
            JsonRpcMethodTest::throw_exception);
    json_rpc::JsonRpcMethod param_copier(
            JsonRpcMethodTest::always_validate,
            JsonRpcMethodTest::write_params_to_response);

    Json::Value params;
    Json::Value response;
    params.append(Json::Value(1));
    ASSERT_THROW(exception_raiser(params, response), std::exception);

    ASSERT_NO_THROW(param_copier(params, response));
    EXPECT_EQ(params, response);

}
