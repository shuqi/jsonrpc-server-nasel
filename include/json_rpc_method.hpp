/*
 * Copyright (c) 2012, Nasel Security
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef JSON_RPC_METHOD_HPP_
#define JSON_RPC_METHOD_HPP_

#include <functional>

#include <json/value.h>

namespace json_rpc {

/**
 * \brief Exception indicating the parameters given are invalid.
 */
class InvalidParameters: public std::exception {};

typedef std::function<bool(const Json::Value&)> param_validator;

typedef std::function<void(const Json::Value&, Json::Value&)> rpc_method;

/**
 * \brief Represent a callable RPC method.
 *
 * This class is used to hold the parameter validation function
 * and the method to execute.
 *
 * When an instance is called it triggers the parameter validation
 * and in case it is successful, it executes the rcp_method.
 *
 */
class JsonRpcMethod {
public:

    /**
     * \brief Construct an object given the parameter validation function and the method.
     *
     * @param validate Function used to check the parameters.
     * @param to_call The actual RPC method routine.
     */
    JsonRpcMethod(param_validator validate, rpc_method to_call);

    /**
     * \brief Copy constructor.
     *
     * @param other
     */
    JsonRpcMethod(const JsonRpcMethod& other);

    /**
     * \brief Trigger the RPC calling.
     *
     * Check the parameters and call the RPC routine.
     *
     * @param params Json containing the parameters.
     * @param response Json response.
     */
    void operator()(const Json::Value& params, Json::Value& response);

private:

    /**
     * \brief Parameter validation function.
     */
    param_validator validate_func_;

    /**
     * \brief RPC method function.
     */
    rpc_method rpc_method_;

};



} /* namespace json_rpc */
#endif /* JSON_RPC_METHOD_HPP_ */
