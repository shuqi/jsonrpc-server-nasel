/*
 * Copyright (c) 2012, Nasel Security
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef JSONRPCPARSER_HPP_
#define JSONRPCPARSER_HPP_

#include "json_rpc_method.hpp"

#include <iostream>
#include <string>
#include <map>

#include <stdint.h>

namespace json_rpc {

typedef std::map<std::string, JsonRpcMethod> method_map;

/**
 * \brief Parse and dispatch the requested RPC methods.
 *
 * This class is in charge of parsing the RPC request received,
 * look up the method and dispatch it.
 *
 * It is this class that actually implements the Json-RPC protocol.
 *
 * In case of error it is in charge of assembling the error response.
 * And if a response is needed to be sent, it assembles it.
 *
 */
class JsonRpcParser {

public:

    /**
     * \brief Parse a received Json-RPC request.
     *
     * Main method of the class. It tries to parse the Json-RPC request
     * from the input stream, processes it and stores the response in
     * the response string.
     *
     * The return value indicates whether there is a response in the
     * string or not.
     *
     * @param input Input stream from where to read the Json-RPC request.
     * @param output The response to be sent back to the client.
     * @return boolean value, if true the response string needs to be sent
     * back to the client, otherwise no response needs to be sent.
     */
    bool parse(std::istream& input, std::string& output);

    /**
     * \brief Add method to be supported by the server.
     *
     * @param method_name The name of the method to be added.
     * @param method_body [JsonRpcMethod](\ref JsonRpcMethod) to be executed.
     */
    void add_method(const std::string& method_name, JsonRpcMethod method_body);

private:

    /**
     * \brief Prepares a JSON-RPC error response.
     *
     * Given the error_code, the error_message and, optionally, the id;
     * return the Json that needs to be sent back indicating the error
     * following the JSON-RPC specification.
     *
     * @param error_code Integer indicating the error code.
     * @param error_message Message explaining the error
     * @param id Indicates the id of the request that produced the error (optional)
     * @return Json to be sent back to the client.
     */
    Json::Value prepare_error(int32_t error_code, std::string error_message, Json::Value id = Json::Value(Json::nullValue));

    /**
     * \brief Prepares a JSON-RPC valid response.
     *
     * Given the called method's resulting Json value and its id
     * generates the Json that needs to be sent back to the client as response,
     * following the JSON-RPC specification.
     *
     * @param result Json to be sent as result of the method.
     * @param id The id that generated this response.
     * @return Json to be sent back to the client.
     */
    Json::Value prepare_response(const Json::Value& result, Json::Value id);

    /**
     * \brief Validates whether the parsed Json follows the JSON-RPC specification.
     *
     * @param root Parsed Json.
     * @return True if it's a valid JSON-RPC request, false otherwise.
     */
    bool validate_rpc(const Json::Value& root);

    /**
     * \brief Parse a single RPC Json request.
     *
     * @param request JSON request to process.
     * @param response JSON response to send back to the client.
     * @return True if there is response or false otherwise.
     */
    bool parse_request(const Json::Value& request, Json::Value& response);

    /**
     * \brief Parse a batch RPC Json request.
     *
     * @param request JSON batch request to process.
     * @param response JSON response to send back to the client.
     * @return True if there is response or false otherwise.
     */
    bool parse_batch(const Json::Value& request, Json::Value& response);

    /**
     * \brief Map to store the available methods.
     */
    method_map methods_;

};

} /* namespace json_rpc */
#endif /* JSONRPCPARSER_HPP_ */
